import React, {useEffect, useState} from 'react';
import {Image, Text, TouchableOpacity, View, FlatList, Alert} from 'react-native';
import styleFile from '../style';
import {useDispatch, useSelector} from 'react-redux';
import {set_survey_result} from '../modules/bono';

const surveyComponent = (props) => {

    const [detail, setDetail] = useState(props.surveyList[props.number]) //뿌려줄 설문 데이터
    const [number, setNumber] = useState(props.number) //설문 인덱스
    const [select, setSelect] = useState(null) //설문 선택항목 인덱스

    const dispatch = useDispatch();
    const surveyResult = useSelector(state => state.bono.surveyResult) //

    useEffect(() => {
        console.log('customerSelectVal :: ', surveyResult)
    },[surveyResult])

    const subTitle = () => {
        let result = `어떤 ${detail.type}을/를 고르실 건가요?`
        return result
    }

    const next = () => {
        if (select == null) {
            Alert.alert(
                "",
                "항목을 선택해 주세요",
                [
                    { text: "OK", onPress: () => null }
                ]
            );
            return;
        } else {
            //number : 설문 인덱스,  selectedId : 설문 선택항목 인덱스
            console.log('selectedId, number :: ', selectedId, number);

            //설문 인덱스에 선택항목 인덱스를 reducer에 업데이트
            surveyResult[number] = selectedId
            dispatch(set_survey_result(surveyResult))

            //해당설문값 초기화
            setSelect(null);
            setSelectedId(null);
        }
        // 세부사항 다음으로 변경
        // navi title 다음으로 변경
        if (props.surveyList.length > number + 1) {
            setNumber(number + 1);
            setDetail(props.surveyList[number + 1]);
            props.naviTypeTitle(props.surveyList[number + 1].type);
            props.setSurveyListNumber(number+1)
        } else {
            props.setSurveyListNumber(number+1)
            Alert.alert(
                "",
                `설문끝. ${surveyResult}`,
                [
                    { text: "OK", onPress: () =>  props.navigation.navigate('surveyResult') }
                ]
            );
        }
    }

    const [selectedId, setSelectedId] = useState(null)

    const SurveyComponentDetail = ({list, number, setSelect}) => {
        const {idx, content1, content2} = list.item

        const check = idx == selectedId ? true : false

        const select = () => {
            setSelect(idx)
            setSelectedId(idx)
        }
        return (
            <TouchableOpacity style={check ? styleFile.surveyDetailBoxTempCheck : styleFile.surveyDetailBoxTemp} onPress={() => select()}>
                <View style={styleFile.surveyDetailBoxDetail}>
                    <View style={{marginTop: 10, marginBottom: 15, marginLeft: 15, marginRight: 15}}>
                        <Image source={check ? require('../asset/images/check-on.png') : require('../asset/images/check-off.png')}/>
                    </View>
                    <View>
                        <Text style={styleFile.surveyDetailBoxContent1}>{content1}</Text>
                        <Text style={styleFile.surveyDetailBoxContent2}>{content2}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View style={{backgroundColor: 'white', flex: 1}}>
            <View>
                <View style={{marginTop: 7, marginBottom: 10}}>
                    <Text style={styleFile.surveyTitle}>{detail.title}</Text>
                    <Text style={styleFile.titleFont2}>{subTitle()}</Text>
                </View>
                <View>
                    <FlatList
                        keyExtractor={item => item.idx}
                        data={JSON.parse(detail.list)} //데이터를 배열형태의 문자열로 받아와서 배열로 파싱
                        renderItem={(item) => <SurveyComponentDetail list={item} number={number} setSelect={setSelect}/>}
                    />
                </View>
            </View>

            <TouchableOpacity style={styleFile.button} onPress={() => next()}>
                <Text style={styleFile.buttonDetail}>다음</Text>
            </TouchableOpacity>
        </View>
    );

}

export default surveyComponent;

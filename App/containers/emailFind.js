import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput, Image,
} from 'react-native';
import styleFile from '../style'

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const emailFind = ({navigation}) => {

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
            <View style={styleFile.background}>
                <View style={styleFile.titlePosition}>
                    <Text style={styleFile.titleFont}>이메일 찾기</Text>
                    <Text style={styleFile.titleFont2}>아래의 기입란에 성명 및 생년월일을 입력 하시면 가입하신 이메일을 찾으실 수 있습니다.</Text>
                </View>
                <View style={styleFile.textInputLayer}>
                    <TextInput style={styleFile.textInput} placeholder="     성명"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     생년월일"></TextInput>

                </View>
                <TouchableOpacity style={styleFile.button}>
                    <Text style={styleFile.buttonDetail}>이메일 찾기</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default emailFind;

const styles = StyleSheet.create({

})

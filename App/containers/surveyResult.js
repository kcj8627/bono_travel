import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';
import styleFile from '../style';

const surveyResult = ({navigation}) => {

    const tempTitleText = 'bonopig님은 힐링 여행가'
    const tempContentText = '여행은 일상의 쉼표, 힐링이라는 말이 가장 잘어울리는 것 같아요. 여행지에서 느끼는 여유 보다 행복한 것이 있을까요?'

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
            <View style={styleFile.background}>
            <View style={styles.imagePosition}>
                <Image source={require('../asset/images/type_adv_m.png')} style={{resizeMode:'contain', width: '100%', height: '55%'}}/>
            </View>
            <View style={styles.textPosition}>
                <View><Text style={styles.titleStyle}>{tempTitleText}</Text></View>
                <View style={styles.contentPosition}><Text style={styles.contentStyle}>{tempContentText}</Text></View>
            </View>
            <View style={styles.graphPosition}>
                <View><Text>그래프 보여줌</Text></View>
            </View>
            <View style={styleFile.button2}>
                <View style={styles.homeButton}>
                    <TouchableOpacity onPress={() => navigation.navigate("MyPage")}>
                        <Text style={styles.font}>돌아가기</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.shareButton}>
                    <TouchableOpacity>
                        <Text style={styles.font}>공유하기</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </View>
        </View>
    );
};

export default surveyResult;

const styles = StyleSheet.create({
    imagePosition: {
        marginTop: 20
    },
    textPosition: {
        marginTop : -150,
        alignItems: 'center'
    },
    titleStyle: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    contentPosition: {
        marginTop: 13,
        marginRight: 30,
        marginLeft: 30,
    },
    contentStyle: {
        fontSize: 14,
        textAlign: 'center'
    },
    graphPosition: {
        marginTop: 13
    },
    homeButton: {
        backgroundColor: '#121A27'
    },
    shareButton: {
        marginLeft: 50,
        backgroundColor: '#00C0E1',
    },
    font: {
        color: 'white',
    },
})



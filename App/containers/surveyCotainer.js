import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import SurveyComponent from '../components/surveyComponent';
import styleFile from '../style';

import * as Progress from 'react-native-progress';

import {useSelector} from 'react-redux';

const surveyContainer = ({navigation}) => {

    const {surveyList} = useSelector(state => state.bono);

    const [surveyListNumber, setSurveyListNumber] = useState(0);

    const [type, setType] = useState(surveyList[0].type)

/*    const nextSurvey = () => {
        let length = surveyList.length;

        // setSurveyListNumber(0);
        console.log(surveyListNumber);
        // setSurveyListNumber(0)
    };*/

    useEffect(() => {
        navigation.setOptions({
            title: type
        })
    },[type])

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
            <View style={styleFile.background}>
                <View style={styleFile.progressBarPosition}>
                    <Progress.Bar progress={(surveyListNumber)/(surveyList.length)}
                                  width={320} color='#00C0E1' borderWidth= {0} unfilledColor='#F5F5F5' animationType={'timing'}/>
                </View>
                <SurveyComponent surveyList={surveyList} number={surveyListNumber} naviTypeTitle={setType}
                                 setSurveyListNumber={setSurveyListNumber} navigation = {navigation}/>

            </View>
        </View>
    );
};

export default surveyContainer;

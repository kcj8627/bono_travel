import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput, Image,
} from 'react-native';
import styleFile from '../style';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const emailLogin = ({navigation}) => {

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
            <View style={styleFile.background}>
                <View style={styleFile.titlePosition}>
                    <Text style={styleFile.titleFont}>이메일로 로그인</Text>
                </View>
                <View>
                    <TextInput style={styleFile.textInput} placeholder="     이메일 주소"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     비밀번호"></TextInput>
                    {/*<TouchableOpacity>
                    <Image source={require('../asset/images/int_eye_close.png')}/>
                </TouchableOpacity>*/}

                </View>
                <View style={styles.findInfo}>
                    <TouchableOpacity onPress={() => navigation.navigate('EmailFind')}>
                        <Text style={styles.findInfoText}>이메일 찾기</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate('PasswordFind')}>
                        <Text style={styles.findInfoText2}>비밀번호를 잊으셨나요</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styleFile.button}>
                    <Text style={styleFile.buttonDetail}>로그인</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default emailLogin;

const styles = StyleSheet.create({
    findInfo: {
        flexDirection: 'row',
    },
    findInfoText: {
        color: '#00C0E1',
    },
    findInfoText2: {
        color: '#00C0E1',
        marginLeft: '15%',
    },
});

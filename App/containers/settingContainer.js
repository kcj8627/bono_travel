import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import styleFile from '../style';

const settingContainer = ({navigation}) => {

    return (
        <View style={{backgroundColor: 'white', flex: 1}}>
            <View style={styleFile.settingPosition}>

                <TouchableOpacity style={styleFile.settingContentPosition}>
                    <Image source={require('../asset/images/profile_edit.png')}/>
                    <Text style={styleFile.settingText}>정보 변경</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styleFile.settingContentPosition}>
                    <Image source={require('../asset/images/password_edit.png')}/>
                    <Text style={styleFile.settingText}>비밀번호 변경</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styleFile.settingContentPosition}>
                    <Image source={require('../asset/images/alarm_edit.png')}/>
                    <Text style={styleFile.settingText}>알림 설정</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styleFile.settingContentPosition}>
                    <Image source={require('../asset/images/add_edit.png')}/>
                    <Text style={styleFile.settingText}>광고 제거</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styleFile.settingContentPosition}>
                    <Image source={require('../asset/images/account_edit.png')}/>
                    <Text style={styleFile.settingText}>계정 연동</Text>
                </TouchableOpacity>

                <Image style={{marginTop: 23, marginBottom: 10}} source={require('../asset/images/line.png')}/>

                <TouchableOpacity style={styleFile.settingContentPosition}>
                    <Image source={require('../asset/images/broadcast_edit.png')}/>
                    <Text style={styleFile.settingText}>공지사항</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styleFile.settingContentPosition}>
                    <Image source={require('../asset/images/QnA_edit.png')}/>
                    <Text style={styleFile.settingText}>문의하기</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{marginTop: 25}}>
                    <Text style={styleFile.settingLogoutText}>로그아웃</Text>
                </TouchableOpacity>


            </View>
            <View style={styleFile.bottomNav}>
                <TouchableOpacity onPress={() => navigation.navigate('MyPage')}>
                    <Image source={require('../asset/images/icon_foot_my_off.png')}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require('../asset/images/icon_foot.png')}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('settingContainer')}>
                    <Image source={require('../asset/images/icon_foot_opt_on.png')}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default settingContainer;

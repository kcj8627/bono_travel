import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput, Image, ScrollView,
} from 'react-native';
import styleFile from '../style';

const myPage = ({navigation}) => {

    return (
        <View style={{backgroundColor: 'white', flex: 1}}>
            <ScrollView contentContainerStyle={{flexGrow: 1}}>
                <View style={{marginLeft: '5%', marginRight: '5%'}}>
                    <View style={styles.header}>
                        <View style={styles.headerProfile}>
                            <TouchableOpacity>
                                <Image source={require('../asset/images/temp_profile_img.png')}/>
                            </TouchableOpacity>
                            <View style={styles.HeaderNickNamePosition}>
                                <Text style={styles.headerNickName}>bonopig</Text>
                                <Text style={styles.headerNickName2}>힐링</Text>
                            </View>
                        </View>
                        <View style={styles.headerRight}>
                            <View style={styles.counter}>
                                <View style={styles.counterDetail}>
                                    <Text style={styles.counterDetailCount}>900</Text>
                                    <Image style={{bottom: '5%'}} source={require('../asset/images/icon_heart.png')}/>
                                </View>
                                <View style={styles.counterDetail}>
                                    <Text style={styles.counterDetailCount}>900</Text>
                                    <Text style={styles.counterDetailText}>팔로잉</Text>
                                </View>
                                <View>
                                    <Text style={styles.counterDetailCount}>900</Text>
                                    <Text style={styles.counterDetailText}>팔로워</Text>
                                </View>
                            </View>
                            <View>
                                <TouchableOpacity style={styles.myCodeEdit}
                                                  onPress={() => navigation.navigate('surveyContainer')}>
                                    <Text style={styles.myCodeEditText}>내 코드수정</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={styles.aboutMe}>
                        <Text style={styleFile.titleFont}>자기소개</Text>
                        <Text style={styleFile.titleFont2}>
                            {`여행을 좋아하는 웹 디자이너
마음이 맞는 여행 친구를 구하고 있어요
인스타아이디 : bonopig`}</Text>
                    </View>
                    <View style={styles.bestCode}>
                        <Text style={styleFile.titleFont}>최고의 코드</Text>
                        <View style={styleFile.friendList}>
                            <View style={styleFile.friendProfile}>
                                <Image source={require('../asset/images/testProfile1.png')}/>
                            </View>
                            <View style={styleFile.friendListInfo}>
                                <Text style={styleFile.friendListInfoName}>Shabrina</Text>
                                <Text style={styleFile.friendListInfoEmail}>Shabrin@gmail.com</Text>
                            </View>
                            <View style={styleFile.friendListInfo2}>
                                <Image style={styleFile.friendListInfo2Icon}
                                       source={require('../asset/images/vector.png')}/>
                                <Text style={styleFile.friendListInfo2Point}>87</Text>
                            </View>
                        </View>
                        <View style={styleFile.friendList}>
                            <View style={styleFile.friendProfile}>
                                <Image source={require('../asset/images/testProfile1.png')}/>
                            </View>
                            <View style={styleFile.friendListInfo}>
                                <Text style={styleFile.friendListInfoName}>Shabrina</Text>
                                <Text style={styleFile.friendListInfoEmail}>Shabrin@gmail.com</Text>
                            </View>
                            <View style={styleFile.friendListInfo2}>
                                <Image style={styleFile.friendListInfo2Icon}
                                       source={require('../asset/images/vector.png')}/>
                                <Text style={styleFile.friendListInfo2Point}>75</Text>
                            </View>
                        </View>
                        <View style={styleFile.friendList}>
                            <View style={styleFile.friendProfile}>
                                <Image source={require('../asset/images/testProfile1.png')}/>
                            </View>
                            <View style={styleFile.friendListInfo}>
                                <Text style={styleFile.friendListInfoName}>Shabrina</Text>
                                <Text style={styleFile.friendListInfoEmail}>Shabrin@gmail.com</Text>
                            </View>
                            <View style={styleFile.friendListInfo2}>
                                <Image style={styleFile.friendListInfo2Icon}
                                       source={require('../asset/images/vector.png')}/>
                                <Text style={styleFile.friendListInfo2Point}>33</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.bestPreference}>
                        <Text style={styleFile.titleFont}>최고의 취향</Text>
                        <View style={styleFile.friendList}>
                            <View style={styleFile.friendProfile}>
                                <Image source={require('../asset/images/testProfile1.png')}/>
                            </View>
                            <View style={styleFile.friendListInfo}>
                                <Text style={styleFile.friendListInfoName}>Shabrina</Text>
                                <Text style={styleFile.friendListInfoEmail}>Shabrin@gmail.com</Text>
                            </View>
                            <View style={styleFile.friendListInfo2}>
                                <Image style={styleFile.friendListInfo2Icon}
                                       source={require('../asset/images/vector.png')}/>
                                <Text style={styleFile.friendListInfo2Point}>33</Text>
                            </View>
                        </View>
                        <View style={styleFile.friendList}>
                            <View style={styleFile.friendProfile}>
                                <Image source={require('../asset/images/testProfile1.png')}/>
                            </View>
                            <View style={styleFile.friendListInfo}>
                                <Text style={styleFile.friendListInfoName}>Shabrina</Text>
                                <Text style={styleFile.friendListInfoEmail}>Shabrin@gmail.com</Text>
                            </View>
                            <View style={styleFile.friendListInfo2}>
                                <Image style={styleFile.friendListInfo2Icon}
                                       source={require('../asset/images/vector.png')}/>
                                <Text style={styleFile.friendListInfo2Point}>33</Text>
                            </View>
                        </View>
                        <View style={styleFile.friendList}>
                            <View style={styleFile.friendProfile}>
                                <Image source={require('../asset/images/testProfile1.png')}/>
                            </View>
                            <View style={styleFile.friendListInfo}>
                                <Text style={styleFile.friendListInfoName}>Shabrina</Text>
                                <Text style={styleFile.friendListInfoEmail}>Shabrin@gmail.com</Text>
                            </View>
                            <View style={styleFile.friendListInfo2}>
                                <Image style={styleFile.friendListInfo2Icon}
                                       source={require('../asset/images/vector.png')}/>
                                <Text style={styleFile.friendListInfo2Point}>33</Text>
                            </View>
                        </View>
                        <View style={styleFile.friendList}>
                            <View style={styleFile.friendProfile}>
                                <Image source={require('../asset/images/testProfile1.png')}/>
                            </View>
                            <View style={styleFile.friendListInfo}>
                                <Text style={styleFile.friendListInfoName}>Shabrina</Text>
                                <Text style={styleFile.friendListInfoEmail}>Shabrin@gmail.com</Text>
                            </View>
                            <View style={styleFile.friendListInfo2}>
                                <Image style={styleFile.friendListInfo2Icon}
                                       source={require('../asset/images/vector.png')}/>
                                <Text style={styleFile.friendListInfo2Point}>33</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>

            <View style={styleFile.bottomNav}>
                <TouchableOpacity>
                    <Image source={require('../asset/images/icon_foot_my_on.png')}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require('../asset/images/icon_foot.png')}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('settingContainer')}>
                    <Image source={require('../asset/images/icon_foot_opt_off.png')}/>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default myPage;

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
    },
    headerProfile: {
        alignItems: 'center',
    },
    HeaderNickNamePosition: {
        alignItems: 'center',
        marginTop: '7%',
    },
    headerNickName: {
        fontSize: 13,
    },
    headerNickName2: {
        fontSize: 12,
        color: 'grey',
    },
    counter: {
        flexDirection: 'row',
        paddingLeft: '14%',
        paddingRight: '10%',
        justifyContent: 'space-between',
    },
    counterDetail: {
        // marginLeft: '25%',
        marginRight: '25%',
        alignItems: 'center',
    },
    counterDetail2: {
        alignItems: 'center',
    },
    counterDetailText: {
        color: 'grey',
    },
    counterDetailCount: {
        fontWeight: 'bold',
        fontSize: 17,
        marginBottom: 7,
    },
    myCodeEdit: {
        marginTop: '5%',
        marginLeft: '10%',
        marginRight: '10%',

    },
    myCodeEditText: {
        paddingBottom: '3%',
        paddingTop: '3%',
        textAlign: 'center',
        // height: '15%'
        borderWidth: 1,
        borderColor: '#E5E5E5',
        borderRadius: 2,
    },
    aboutMe: {
        marginTop: 25,
    },
    bestCode: {
        marginTop: 25,
        // marginBottom: 50
    },
    bestPreference: {
        marginTop: 25,
        marginBottom: 60
    }
});

import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput, Image,
} from 'react-native';
import styleFile from '../style';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const passwordFind = ({navigation}) => {

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
        <View style={styleFile.background}>
            <View style={styleFile.titlePosition}>
                <Text style={styleFile.titleFont}>비밀번호 찾기</Text>
                <Text style={styleFile.titleFont2}>계정으로 사용 하시는 이메일 주소를 입력하시면 임의로 설정된 비밀번호를 발송해 드립니다</Text>
            </View>
            <View style={styles.textInputLayer}>
                <TextInput style={styleFile.textInput} placeholder="     이메일 주소"></TextInput>

            </View>
                <TouchableOpacity style={styleFile.button}>
                    <Text style={styleFile.buttonDetail}>이메일 찾기</Text>
                </TouchableOpacity>
        </View>
        </View>
    )
}

export default passwordFind;

const styles = StyleSheet.create({
    textInputLayer: {
        marginTop: 15
    },
    findInfo: {
        top: hp('6%'),
        flexDirection:'row',
    },
    findInfoText: {
        color: '#00C0E1',
        marginLeft: 35
    }
})

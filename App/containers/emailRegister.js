import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput, Image,
} from 'react-native';
import styleFile from '../style'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const emailRegister = ({navigation}) => {

    return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
            <View style={styleFile.background}>
                <View style={styleFile.titlePosition}>
                    <Text style={styleFile.titleFont}>이메일로 가입하기</Text>
                </View>
                <View>
                    <TextInput style={styleFile.textInput} placeholder="     이메일 주소"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     성명"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     별명"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     생년월일"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     성별"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     비밀번호"></TextInput>
                    <TextInput style={styleFile.textInput} placeholder="     비밀번호 확인"></TextInput>
                </View>
                <View style={styles.agreePosition}>
                    <Text style={styles.agreeText}>가입하면 code의 약관, 데이터 정책 및</Text>
                    <Text style={styles.agreeText}>쿠키정책에 동의하게 됩니다</Text>
                </View>
                <TouchableOpacity style={styleFile.button}>
                    <Text style={styleFile.buttonDetail}>로그인</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default emailRegister;

const styles = StyleSheet.create({
    agreeText: {
        color: 'gray',
        fontSize: 13,
        marginLeft: '12%',
        marginRight: '12%',
        textAlign: 'center',
    },
    agreePosition: {
        marginTop: 30
    }
});

import kakaoLogins from '@react-native-seoul/kakao-login'

const login = () => {
    kakaoLogins.login().then(result => {
        kakaoLogins.getProfile().then(result =>
        {
            console.log(`${JSON.stringify(result)}`)
            return result;
        }).catch(err => {
            console.log(`${JSON.stringify(result)}`)
        })
    }).catch(err => {
        console.log(`${JSON.stringify(err)}`)
    })
}

const logout = () => {
    kakaoLogins.logout().then(result => {
        console.log(`${JSON.stringify(result)}`)
    }).catch(err => {
        console.log(`${JSON.stringify(err)}`)
    })
}

module.exports = {
    login: login,
    logout: logout
};

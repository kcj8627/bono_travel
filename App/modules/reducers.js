import {combineReducers} from 'redux';
import bono from './bono'

const rootReducer = combineReducers({
    bono,
});

export default rootReducer;

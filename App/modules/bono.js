const GET_SURVEY_LIST = 'bono/GET_SURVEY_LIST';
const SET_SURVEY_RESULT = 'bono/SET_SURVEY_RESULT';

export const get_survey_list = (list) => ({
    type: GET_SURVEY_LIST,
    payload: list,
});

export const set_survey_result = (list) => ({
    type: SET_SURVEY_RESULT,
    payload: list,
});

const initailState = {
    surveyList: [],
    surveyResult: [],
};

const bono = (state = initailState, action) => {
    switch (action.type) {
        case GET_SURVEY_LIST:
            return {
                ...state,
                surveyList: action.payload,
            };
        case SET_SURVEY_RESULT:
            return {
                ...state,
                surveyResult: action.payload,
            };
        default:
            return state;
    }
};

export default bono;

import React from 'react';
import {StyleSheet} from 'react-native';

//color
const brand = '#00C0E1'
const white = '#FFFFFF'
const pink = '#FF6C6C'
const black = '#1E1A27'
const icon_gray = '#C7C7CC'
const hint = '#BCBBC2'
const line_int = '#E5E5E5'
const info = '#7B7B81'
const white_light = '#FDFDFD'
const icon_bg = '#F5F5F5'

//font_size
const font_default = 15
const font_title = 19
const font_info = 13
const font_number = 19

const styles = StyleSheet.create({
    textInput: {
        borderWidth: 1,
        borderColor: white,
        marginBottom: 10,
        borderRadius: 2,
        height: 45
    },
    titleFont: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 17,
    },
    titleFont2: {
        fontSize: 13,
        color: info
    },
    background: {
        backgroundColor: white,
        flex: 1,
        marginLeft: '10%',
        marginRight: '10%',
    },
    buttonDetail: {
        color: white,
        fontSize: 17,
        fontWeight: 'bold'
    },
    titlePosition: {
        marginTop: 22
    },
    button: {
        marginBottom: 27,
        position: 'absolute',
        bottom:0,
        width: '100%',
        justifyContent: 'center',
        backgroundColor: '#00C0E1',
        height: '8%',
        alignItems: 'center',
        borderRadius: 7,
    },
    button2: {
        marginBottom: 27,
        position: 'absolute',
        bottom:0,
        width: '100%',
        justifyContent: 'center',
        // backgroundColor: '#00C0E1',
        height: '8%',
        alignItems: 'center',
        borderRadius: 7,
        flexDirection: 'row',

    },
    textInputLayer: {
        marginTop: 20
    },
    friendList: {
        flexDirection:'row',
        marginBottom: 19
    },
    friendProfile: {
        width: '15%'
    },
    friendListInfo: {
        width: '68%',
        marginLeft: '2%'
    },
    friendListInfoName:{
        fontWeight: 'bold',
        marginBottom: 3,
        marginTop: 2,
        fontSize: 15,
    },
    friendListInfoEmail: {
        fontSize: 13,
        color: '#7B7B81'
    },
    friendListInfo2: {
        flexDirection:'row',
        width: '15%',
        justifyContent: 'center',
        marginTop: 10
    },
    friendListInfo2Point: {
        fontWeight: 'bold',
        marginLeft: 10,
        fontSize: 18
    },
    friendListInfo2Icon: {
        marginBottom: 10
    },
    bottomNav: {
        position: 'absolute',
        flexDirection:'row',
        bottom:0,
        width: '100%',
        backgroundColor: 'white',
        height: '8%',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: '15%',
        paddingLeft: '15%'
    },
    progressBarPosition: {
        marginTop:13,
        marginBottom: 22
    },
    surveyTitle: {
        fontSize: 19,
        marginBottom: 10
    },
    surveyDetailPosition: {
        marginTop: 20,
    },
    surveyDetailBox: {
        borderWidth: 1,
        borderColor: '#E5E5E5',
        marginBottom: 13,
        borderRadius: 6,
    },
    surveyDetailBoxTemp: {
        borderWidth: 1,
        borderColor: '#7B7B81',
        marginBottom: 10,
        borderRadius: 6,
    },
    surveyDetailBoxTempCheck: {
        borderWidth: 1,
        borderColor: '#00C0E1',
        marginBottom: 10,
        borderRadius: 6,
    },
    surveyDetailBoxDetail:{
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
    },
    surveyDetailBoxContent1: {
        fontSize: 15,
        marginBottom: 2,
        marginTop: 2
    },
    surveyDetailBoxContent2: {
        fontSize: 13,
        color: '#7B7B81'
    },
    settingContentPosition: {
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 20
    },
    settingText: {
        marginLeft: 14,
        // fontWeight: 'bold',
        fontSize: 15
    },
    settingLogoutText: {
        fontSize: 15,
        color: '#00C0E1'
    },
    settingPosition: {
        marginLeft: '5%',
    }
});

export default styles;

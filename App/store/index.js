import AsyncStorage from '@react-native-community/async-storage';
import {persistStore, persistReducer} from 'redux-persist';
import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../modules/reducers';
import logger from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
let store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(logger)),
);
let persistor = persistStore(store);

export {store, persistor};

import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Image, TouchableOpacity, Alert,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import EmailLogin from './containers/emailLogin'
import EmailFind from './containers/emailFind'
import PasswordFind from './containers/passwordFind'
import EmailRegister from './containers/emailRegister'
import MyPage from './containers/myPage'
import SettingContainer from './containers/settingContainer';
import SurveyContainer from './containers/surveyCotainer';
import SurveyResult from './containers/surveyResult'

import TouchableItem from '@react-navigation/stack/src/views/TouchableItem';
import {useDispatch, useSelector} from 'react-redux';

import styleFile from './style';

import {
    KakaoOAuthToken,
    KakaoProfile,
    getProfile as getKakaoProfile,
    login,
    logout,
    unlink,
} from '@react-native-seoul/kakao-login';

import api from './api/testData'
import {get_survey_list} from './modules/bono';

const LoginStart = ({navigation}) => {

    const {surveyList} = useSelector(state => state.bono);
    const dispatch = useDispatch();

    const getSurvey = async () => {
        let result = await api.getSurvey()
        dispatch(get_survey_list(result));
        return result
    }
    useEffect(() => {
        getSurvey().then(r => console.log('항목 세팅완료 :: ', r));
    },[])


    /*****카카오로그인 관련 코드 시작*****/
    const [result, setResult] = useState('');
    const signInWithKakao = async Promise => {

        const token = await login();
        console.log(token)

        setResult(JSON.stringify(token));
    };
    const signOutWithKakao = async Promise => {
        const message = await logout();
        console.log(message)

        setResult(message);
    };
    const getProfile = async Promise => {
        try {
            const profile = await getKakaoProfile();
            console.log(JSON.stringify(profile))
            setResult(JSON.stringify(profile));
        } catch (e) {
            console.log(e);
        }
    };
    /*****카카오로그인 관련 코드 종료*****/

    return (
        <ImageBackground source={require('./asset/images/img_login.png')} style={styles.bgImage}>

            {/*<Text>{result}</Text>
            <TouchableItem onPress={() => signInWithKakao()}>
                <Text>카카오로그인테스트</Text>
            </TouchableItem>
            <TouchableItem onPress={() => signOutWithKakao()}>
                <Text>카카오로그아웃테스트</Text>
            </TouchableItem>
            <TouchableItem onPress={() => getProfile()}>
                <Text>카카오getProfile</Text>
            </TouchableItem>*/}

            <View style={styles.code}>
                <Image style={styles.icon} source={require('./asset/images/logo.png')}/>
                <TouchableItem style={styles.code} onPress={() => navigation.navigate('MyPage')}>
                    <Text>마이페이지 이동</Text>
                </TouchableItem>
            </View>

            <View style={styles.logo}>
                <TouchableItem onPress={()=> navigation.navigate("EmailLogin")}>
                    <Image style={styles.logoDetail} source={require('./asset/images/icon_mail_login.png')}/>
                </TouchableItem>
                <TouchableItem onPress={() => signInWithKakao()}>
                    <Image style={styles.logoDetail} source={require('./asset/images/icon_kakao.png')}/>
                </TouchableItem>
                {/*<Image style={styles.logoDetail} source={require('./asset/images/icon_naver.png')}/>
                <Image style={styles.logoDetail} source={require('./asset/images/icon_facebook.png')}/>*/}
            </View>
            <View style={styles.email}>
                <TouchableItem onPress={()=> navigation.navigate("EmailRegister")}>
                    <Text style={styles.emailFont}>이메일로 가입하기</Text>
                </TouchableItem>
            </View>
        </ImageBackground>
    )
}

const App = () => {
    const Stack = createStackNavigator();

    return (
        <View style={{flex: 1}}>
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen
                        name="LoginStart"
                        component={LoginStart}
                        options={{headerShown: false}}
                    />
                    <Stack.Screen
                        name="EmailLogin"
                        component={EmailLogin}
                        /*options={{title: '', headerShown: true
                            /!*,header: ({goBack}) => (
                                <TouchableOpacity onPress={ () => { goBack() } }>
                                    <Image source={require('./asset/images/icon_nav_close.png')}  />
                                </TouchableOpacity>
                            )*!/
                        }}*/
                        options={({navigation, route}) => ({
                            title: null,
                            headerStyle: {
                                shadowColor: 'transparent'
                            },
                            // headerLeft: null,
                            headerLeft: (props) => (
                                <TouchableOpacity style={{marginLeft: 20}} onPress={() => {
                                    navigation.goBack();
                                }}>
                                    <Image source={require('./asset/images/close.png')}/>
                                </TouchableOpacity>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="EmailFind"
                        component={EmailFind}
                        options={({navigation, route}) => ({
                            title: null,
                            headerStyle: {
                                shadowColor: 'transparent'
                            },
                            headerLeft: (props) => (
                                <TouchableOpacity style={{marginLeft: 20}} onPress={() => {
                                    navigation.goBack();
                                }}>
                                    <Image source={require('./asset/images/back.png')}/>
                                </TouchableOpacity>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="PasswordFind"
                        component={PasswordFind}
                        options={({navigation, route}) => ({
                            title: null,
                            headerTitleAlign: 'center',
                            headerLeft: (props) => (
                                <TouchableOpacity style={{marginLeft: 20}} onPress={() => {
                                    navigation.goBack();
                                }}>
                                    <Image source={require('./asset/images/back.png')}/>
                                </TouchableOpacity>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="EmailRegister"
                        component={EmailRegister}
                        options={({navigation, route}) => ({
                            title: null,
                            headerTitleAlign: 'center',
                            headerLeft: (props) => (
                                <TouchableOpacity style={{marginLeft: 20}} onPress={() => {
                                    navigation.goBack();
                                }}>
                                    <Image source={require('./asset/images/back.png')}/>
                                </TouchableOpacity>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="MyPage"
                        component={MyPage}
                        options={({navigation, route}) => ({
                            title: '마이 페이지',
                            headerTitleStyle: {
                                fontWeight: 'bold',
                                fontSize: 17,
                            },
                            headerTitleAlign: 'center',
                            headerStyle: {
                                shadowColor: 'transparent'
                            },
                            headerLeft: null,
                            headerRight: (props) => (
                                <TouchableOpacity style={{marginRight: 20}} onPress={() => {
                                    navigation.goBack();
                                }}>
                                    <Image source={require('./asset/images/icon_nav_alarm.png')}/>
                                </TouchableOpacity>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="settingContainer"
                        component={SettingContainer}
                        options={({navigation, route}) => ({
                            title: '설정',
                            headerTitleStyle: {
                                fontWeight: 'bold',
                                fontSize: 17,
                                alignSelf: 'center',
                            },
                            headerStyle: {
                                shadowColor: 'transparent'
                            },
                            headerTitleAlign: 'center',
                            headerLeft: null,
                        })}
                    />
                    <Stack.Screen
                        name="surveyContainer"
                        component={SurveyContainer}
                        options={({navigation, route}) => ({
                            headerTitleStyle: {
                                fontWeight: 'bold',
                                fontSize: 17,
                            },
                            headerTitleAlign: 'center',
                            headerStyle: {
                                shadowColor: 'transparent'
                            },
                            headerLeft: (props) => (
                                <TouchableOpacity style={{marginLeft: 20}} onPress={() => {
                                    // navigation.goBack();
                                    console.log('이전문항으로 기능 구현 필요')
                                }}>
                                    <Image source={require('./asset/images/back.png')}/>
                                </TouchableOpacity>
                            ),
                            headerRight: (props) => (
                                <TouchableOpacity style={{marginRight: 20}} onPress={() => {
                                    Alert.alert(
                                        "",
                                        "홈으로 돌아가시겠습니까",
                                        [
                                            { text: "OK", onPress: () => navigation.goBack() },
                                            { text: "NO", onPress: () => null }
                                        ]
                                    );
                                }}>
                                    <Image source={require('./asset/images/home.png')} style={{width: 27, height: 27}}/>
                                </TouchableOpacity>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="surveyResult"
                        component={SurveyResult}
                        options={({navigation, route}) => ({
                            title: '코드생성',
                            headerTitleStyle: {
                                fontWeight: 'bold',
                                fontSize: 17,
                            },
                            headerTitleAlign: 'center',
                            headerStyle: {
                                shadowColor: 'transparent'
                            },
                            headerLeft: null,
                        })}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </View>
    );
};

const styles = StyleSheet.create({
    bgImage: {
        position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,
    },
    iconView: {
        right: 70
    },
    icon: {
        position: 'absolute',
        flexDirection:"column"
    },
    code: {
        top: hp('43%'),
        alignItems: 'center'
    },
    codeFont: {
        fontSize: 30,
        fontWeight: 'bold',
    },
    logo: {
        top: hp('55%'),
        justifyContent: "center",
        // alignItems: 'center',
        flexDirection:'row',
    },
    logoDetail: {
        marginRight: 7.15,
        marginLeft : 7.15
    },
    email: {
        top: hp('80%'),
        alignItems: 'center'
    },
    emailFont: {
        color: '#00C0E1',
        fontSize: 15,
        // fontWeight: 'bold'
    },
});

export default App;

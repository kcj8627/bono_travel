import axios from 'axios';

/*const getSurvey = () => {
    let result = [
        {
            id: '1',
            type: '숙소',
            title: '숙소숙소',
            list: [
                {"idx": 0, "content1": "11111", "content2": "222222"},
                {"idx": 1, "content1": "11111", "content2": "222222"},
                {"idx": 2, "content1": "11111", "content2": "222222"},
                {"idx": 3, "content1": "11111", "content2": "222222"},
                {"idx": 4, "content1": "11111", "content2": "222222"},
                {"idx": 5, "content1": "11111", "content2": "222222"},
            ],
        },
        {
            id: '2',
            type: '음식',
            title: '음식음식',
            list: [
                {"idx": 0, "content1": "11111", "content2": "222222"},
                {"idx": 1, "content1": "11111", "content2": "222222"},
                {"idx": 2, "content1": "11111", "content2": "222222"},
                {"idx": 3, "content1": "11111", "content2": "222222"},
                {"idx": 4, "content1": "11111", "content2": "222222"},
            ],
        },
        {
            id: '3',
            type: '이동수단',
            title: '이동수단이동수단',
            list: [
                {"idx": 0, "content1": "11111", "content2": "222222"},
                {"idx": 1, "content1": "11111", "content2": "222222"},
                {"idx": 2, "content1": "11111", "content2": "222222"},
                {"idx": 3, "content1": "11111", "content2": "222222"}
            ],
        },
    ];

    return result
};*/

const getSurvey = async () => {
    console.log('getSurveyTest 진입');
    let response = await axios.request({
        url: encodeURI('/getSurvey'),
        method: 'get',
        baseURL: 'http://192.168.180.55:3000',
    })
        .catch(error => {
            console.log('getSurveyTest 오류 :: ', error);
            return error;
        });

    console.log('getSurveyTest response :: ', response)
    return response.data;
};



const api = {
    getSurvey
}

export default api;

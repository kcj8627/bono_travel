import {AppRegistry} from 'react-native';
import App from './App/App';
import {name as appName} from './app.json';
import React from 'react';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {store, persistor} from './App/store/index';

const AppRedux = () => (
    <Provider store={store}>
        <PersistGate persistor={persistor} loading={null}>
            <App />
        </PersistGate>
    </Provider>
);
AppRegistry.registerComponent(appName, () => AppRedux);
